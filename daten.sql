SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `Buchung`;
CREATE TABLE `Buchung` (
                           `id` int NOT NULL AUTO_INCREMENT,
                           `gastId` int NOT NULL,
                           `zimmerId` int NOT NULL,
                           `datum` date NOT NULL,
                           `preis` int NOT NULL,
                           PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `Buchung` (`id`, `gastId`, `zimmerId`, `datum`, `preis`) VALUES
                                                                         (1,	1,	2,	'2022-03-05',	78),
                                                                         (2,	2,	1,	'2013-06-20',	60),
                                                                         (3,	3,	3,	'2022-03-05',	50);

DROP TABLE IF EXISTS `Gast`;
CREATE TABLE `Gast` (
                        `id` int NOT NULL AUTO_INCREMENT,
                        `vorname` varchar(40) DEFAULT NULL,
                        `nachname` varchar(40) NOT NULL,
                        `wunschzimmer` int DEFAULT NULL,
                        PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `Gast` (`id`, `vorname`, `nachname`, `wunschzimmer`) VALUES
                                                                     (1,	'Magdalena',	'Düser',	2),
                                                                     (2,	'Fred',	'Vollmer',	NULL),
                                                                     (3,	'Angelika',	'Düser',	3),
                                                                     (4,	'Till',	'Düser',	NULL);

DROP TABLE IF EXISTS `Zimmer`;
CREATE TABLE `Zimmer` (
                          `id` int NOT NULL AUTO_INCREMENT,
                          `groesse` int NOT NULL,
                          `ausblick` varchar(10) NOT NULL,
                          `allergiker` bit(1) NOT NULL DEFAULT b'0',
                          PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `Zimmer` (`id`, `groesse`, `ausblick`, `allergiker`) VALUES
                                                                     (1,	50,	'nord',	CONV('0', 2, 10) + 0),
                                                                     (2,	70,	'sued',	CONV('1', 2, 10) + 0),
                                                                     (3,	20,	'west',	CONV('1', 2, 10) + 0),
                                                                     (4,	10,	'ost',	CONV('0', 2, 10) + 0);