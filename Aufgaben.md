# Aufgaben
## SQL
* Ermittle die Id des Zimmers mit der größten Größe (Fläche).
* Ermittle den Gast (Vorname, Nachname), der das größte Zimmer hat.
* Ermittle die Gäste (Vorname, Nachname), die ihr Wunschzimmer erhalten haben.
* Ermittel die Ids der Zimmer, die bereits gebucht sind.
* Ermittel die Ids der Zimmer, die nicht gebucht sind.
* Ermittel zu einem Gast die gesamten Kosten für all seine gebuchten Zimmer.

## Modellierung
* Zeichne für das bisherige Hotel-Model ein ERM.  
* Entwirf eine neue Entity `Rechnung` und füge sie in das ERM ein.
* Erstelle eine Tabelle `Rechnung` (evtl. auch `Rechnung_Buchung`) in dem Tool, das auf http://localhost:8080 (in den Browser oben kopieren) läuft. Username: root, Passwort: admin, Datenbank: magdalena.
* Füge eine Rechnung zu Magdalena in die Datenbank ein.

## Java
* Schreibe eine Methode `Rechnung getRechnung(String vorname, String nachname)` in Java, die zum einem Gast anhand seines Vor- und Nachnamens die Rechnung(en) ausliest. Was ist gefährlich an dem Vorgehen?
* Schreibe ein SQL, das überprüft, ob es evtl. mehr als einen Gast mit einem gegebene Vor- und Nachnamen gibt.
* Schreibe ein Java-Methode, die das checkt.
* Füge dies als Sicherheitsüberprüfung in die Methode `getRechnung` (s.o.) ein.