package eu.dueser.database;

public class Gast {

    Integer id;
    String vorname;
    String nachname;
    Integer wunschzimmer;

    public Gast(String id, String vorname, String nachname, String wunschzimmer) {
        this.id = Integer.parseInt(id);
        this.vorname = vorname;
        this.nachname = nachname;
        this.wunschzimmer = wunschzimmer == null ? null : Integer.parseInt(wunschzimmer);
    }


    @Override
    public String toString() {
        return "Gast{" +
                "id=" + id +
                ", vorname='" + vorname + '\'' +
                ", nachname='" + nachname + '\'' +
                ", wunschzimmer=" + wunschzimmer +
                '}';
    }
}
