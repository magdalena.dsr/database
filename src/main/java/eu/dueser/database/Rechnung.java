package eu.dueser.database;

public class Rechnung {

    int id;
    int buchungsId;

   public Rechnung(int pId, int pBuchungsId){
       id = pId;
       buchungsId = pBuchungsId;
    }

    @Override
    public String toString() {
        return "Rechnung{" +
                "id=" + id +
                ", buchungsId=" + buchungsId +
                '}';
    }
}
