select Gast.vorname, Gast.nachname, Zimmer.groesse from Gast join Buchung on Gast.id = Buchung.gastId join Zimmer on Buchung.zimmerId = Zimmer.id where wunschzimmer is not null;

insert into Buchung (gastId, zimmerId, datum, preis) values (4, 4, now(), 30);

Select * from Buchung;

select vorname, nachname, groesse from Gast left join Zimmer on Gast.wunschzimmer = Zimmer.id
union select vorname, nachname, groesse from Gast right join Zimmer on Gast.wunschzimmer = Zimmer.id;

select * from Gast;

select * from Zimmer where groesse = (select MAX(groesse) from Zimmer);


update Gast set wunschzimmer = 3 where vorname = 'Angelika'
