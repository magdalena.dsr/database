package eu.dueser.list;


import eu.dueser.database.List;

/**
 * Beschreiben Sie hier die Klasse ADTAufgaben.
 *
 * @author Achim Kassemeier
 * @version 23.11.2016
 */
public class ADTAufgaben {
    // Instanzvariablen - ersetzen Sie das folgende Beispiel mit Ihren Variablen
    private List<String> list;

    /**
     * Konstruktor für Objekte der Klasse ADTAufgaben
     */
    public ADTAufgaben() {
        list = new List<>();
    }

    /**
     * leicht
     */
    public void fuelleListeMitbeliebigenElementen() {
        list.append("Hallo");
        list.append("STinker");

    }

    /**
     * leicht
     */
    public void haengeAnListeAn(String text) {
        list.append(text);

    }

    /**
     * leicht
     */
    public void loescheDasErsteElementDerListe() {
        list.toFirst();
        list.remove();

    }

    /**
     * leicht
     */
    public void loescheDasLetzeElementDerListe() {
        list.toLast();
        list.remove();

    }

    /**
     * Mittel
     */
    public String sucheElementXinDerListeUndGibEsZurueck(String x) {
        list.toFirst();
        while (list.hasAccess()) {
            if (list.getContent().equals(x)) {
                return list.getContent();
            }
            list.next();
        }
        return "Sie haben verkackt";
    }


    /**
     * Mittel
     */
    public void fuegeElementXVorElementYinDieListeEin(String x, String y) {
        list.toFirst();
        while (list.hasAccess()) {
            if (list.getContent().equals(y)) {
                list.insert(x);
            }
            list.next();
        }

    }

    public List<String> gibListe() {
        return list;
    }

    /**
     * leicht: Die aktuellen Inhalte der Liste bleiben erhalten
     */
    public void kopiereUebergebeneListeInDieListe(List<String> newList) {
        list.concat(newList);

    }

    /**
     * leicht: Ausgabe über System.out.println(liste.getContent());
     */
    public void gibAlleElementeDerListeAus() {
        list.toFirst();
        while (list.hasAccess()) {
            System.out.println(list.getContent());
        }
    }


    /**
     * leicht: verschieben heißt an einer Stelle löschen und woanders neu hinzufügen
     */
    public void verschiebeDasErsteElementAnDasEndeDerListe() {
        list.toFirst();
        String content = list.getContent();
        list.remove();
        list.append(content);
    }

    /**
     * leicht
     */
    public void verschiebeDasLetzteElementAnDenAnfangDerListe() {
        list.toLast();
        String hahahh = list.getContent();
        list.remove();
        list.toFirst();
        list.insert(hahahh);

    }

    /**
     * mittel
     */
    public int zaehleAlleElementeUndGibDieAnzahlZurueck() {
        int zaehler = 0;
        while (list.hasAccess()) {
            zaehler++;
        }
        return zaehler;
    }

    /**
     * schwer: Ziel ist es die Liste möglichst zufällig zu durchmischen - Keine weiteren Vorgaben
     */
    public void mischeListe() {
        int size = zaehleAlleElementeUndGibDieAnzahlZurueck();
        for (int i = 0; i < size; i++){
            int pos = (int) (Math.random() * size);
            list.toFirst();
            for (int j = 0; j < pos; j++) {
                list.next();
            }
            String content = list.getContent();
            list.remove();
            list.append(content);
        }

    }

    /**
     * schwer: Keine weiteren Vorgaben
     */
    public void invertiereListe() {
        List<String> tempList = new List<>();
        list.toLast();
        while (list.hasAccess()){
            String objekt = list.getContent();
            list.remove();
            list.toLast();
            tempList.append(objekt);
        }
        tempList.toFirst();
        list = tempList;
    }

    /**
     * schwer
     */
    public void loescheAlleDoppeltenElementeAusDerListe() {

    }

    /**
     * mittel: Fuehre die vorhandene Liste mit der übergebenen nach dem Reisverschlussprinzip zusammen
     */
    public void fuehreListenZusammen(List<String> newList) {

    }
}

