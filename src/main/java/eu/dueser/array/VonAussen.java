package eu.dueser.array;

import eu.dueser.array.MyArrays;

public class VonAussen {

    public static void main(String[] args) {
        MyArrays myArrays = new MyArrays();
        System.out.println(myArrays.arr[0].length);
        myArrays.arr[0][0] = "Magdalena";
        myArrays.arr[3][0] = "Fred";
        myArrays.hallo();
    }

}
