package eu.dueser.array;

public class MyArrays {

    final static int ROWS = 10;
    final static int COLS = 11;
    public String[][] arr = new String[ROWS][COLS];

    void hallo() {
        System.out.println("Hier gebe ich das Array aus:");
        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLS; j++) {
                System.out.print(arr[i][j] + " **");
            }
            System.out.println();
        }

    }


}
